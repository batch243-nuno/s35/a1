const express = require('express');
const app = express();

const mongoose = require('mongoose');
const port = 3002;

mongoose.connect(
  'mongodb+srv://admin:admin@zuittbatch243.1muzoob.mongodb.net/?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log("We're connected to cloud database"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.listen(port, () => console.log(`Server is running in localhost:${port}`));

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});

const User = mongoose.model('Users', userSchema);

app.post('/signup', (request, response) => {
  User.findOne({ username: request.body.username }, (err, result) => {
    if (result != null && result.username == request.body.username) {
      return response.send(`${request.body.username} is Already exist.`);
    } else {
      let newUser = new User({
        username: request.body.username,
        password: request.body.password,
      });
      newUser.save((err, addedUser) => {
        if (err) {
          return console.log(err);
        } else {
          return response
            .status(201)
            .send(`New User ${request.body.username} added.`);
        }
      });
    }
  });
});
